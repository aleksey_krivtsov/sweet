This file is for you to describe the sweet application. Typically
you would include information such as the information below:

Installation and Setup
======================

Install ``sweet`` using easy_install::

    easy_install sweet

Make a config file as follows::

    paster make-config sweet config.ini

Tweak the config file as appropriate and then setup the application::

    paster setup-app config.ini

Then you are ready to go.
