define(
    [
        'jquery',
        'semantic',
        'select'
        //'selectru'
    ],
    function($){
        $('.ui.form')
            .form({
                title: {
                    identifier  : 'title',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please enter title'
                        }
                    ]
                }
            });

		$('.search').on('click', function(){
			var host = document.location.host,
				val = $('.menu.icon.input').find('input').val();
				console.log(host, val);
			location.href = 'http://' + host + '/admin/search/' + val;
        })

        $(".multy").each(function(){
            var vals = [];
            if( $(this).attr('ctype') && dict[$(this).attr('ctype')] )
                vals = dict[$(this).attr('ctype')];
            if(  $(this).attr('field-type') && dict[$(this).attr('field-type')] )
                vals = dict[$(this).attr('field-type')];


            $(this).select2({
                tags: vals,
                tokenSeparators: [","]}
            );
        });
            $('#addFields').on('click', function(){
                $('#field-name').show();

            });
        $('#addField').on('click', function(){
            var rand = Math.floor((Math.random()*1000)+1);
            $('#field-name').hide();
            var label = $('#field-name').find('input').val();
            $('#addd').append('<div id="add-field'+rand+'" ><label></label><input name="" type="text" value="" ctype="casts" field-type="'+label+'" /></div>');
            $('#add-field'+rand).find('input').attr('name', 'new' + rand).addClass('new-fields');
            $('#add-field'+rand).find('label').html(label);
            $('#field-name').find('input').val('');
            $.ajax(
                {
                    url: '/admin/getCastContent',
                    data:
                    {
                        cast: label
                    }
                }
            ).done(
                function(response)
                {
                    console.log(response);
                    $('#add-field'+rand).find('input').select2(
                        {
                            tags: $.parseJSON(response),
                            tokenSeparators: [","]
                        }
                    );
                }
            )

        });

        $('.submit.button').click(
            function()
            {
                if( !$.trim($('input[name="title"]')) )
                    return false;
                var form = $('#edit-hidden-form');

                form.html('<input type="hidden" name="id" value="'+form.attr('data-id')+'">')
                var studios = $('input[ctype="studios"]').val().split(',');
                for( s in studios )
                {
                    form.append('<input type="hidden" name="studios[]" value="'+studios[s]+'">');
                }

                var countries = $('input[ctype="countries"]').val().split(',');
                for( s in countries )
                {
                    form.append('<input type="hidden" name="countries[]" value="'+countries[s]+'">');
                }

                var years = $('input[ctype="years"]').val().split(',');

                for( s in years )
                {
                    form.append('<input type="hidden" name="years[]" value="'+years[s]+'">');
                }

                var casts = $('input[ctype="casts"]').each(
                    function(){
                        vals = $(this).val().split(',');
                        var castname = $(this).attr('field-type');
                        for( c in vals )
                        {
                            form.append('<input type="hidden" name="casts['+castname+'][]" value="'+vals[c]+'">');
                        }
                    }
                );


                return true;
            }
        );
        
    }
)