requirejs.config({
    baseUrl: '/js/',
    paths: {
        jquery         : 'lib/jquery',
        underscore     : 'lib/underscore',
        less           : 'lib/less',
        semantic       : 'lib/semantic',
        'select'       : 'lib/select2',
        'selectru'     : 'lib/select'
    },
    shim : {
        underscore: {
            exports: '_'
        }
    }
});