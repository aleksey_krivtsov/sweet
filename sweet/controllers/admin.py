from sweet.lib.base import BaseController
from couchbase import Couchbase
from couchbase.views.iterator import View
from pylons import url
from pylons.controllers.util import redirect
from pylons.templating import render_jinja2 as render
from pylons import app_globals as g
from cgi import FieldStorage
from elasticsearch import Elasticsearch
import httplib
import hashlib
import os.path
import re
import json


class AdminController(BaseController):
    _couchdbConnect = 0
    _couchdbIdsConnect = 0
    _castsCache = 0
    _perPage = 10

    def _getConnect(self):
        if not self._couchdbConnect:
            self._couchdbConnect = Couchbase.connect(bucket=g.database.get('bucket'), host=g.database.get('host'),
                                                     port=g.database.get('port'))
        return self._couchdbConnect

    def _getIdsConnect(self):
        if not self._couchdbIdsConnect:
            self._couchdbIdsConnect = Couchbase.connect(bucket=g.database.get('ids_bucket'),
                                                        host=g.database.get('host'), port=g.database.get('port'))
        return self._couchdbIdsConnect

    def list(self, id=1):  #Это page на самом деле :)

        page = id if self._isInt(id) and int(id) > 0 else 1
        c = self._getConnect()
        count_total = self._getCountTotal()
        result = View(c, 'dev_catalog', 'list', limit=self._perPage, skip=(int(page) - 1) * self._perPage)

        return render('list.html',
                      {'result': result, 'this': self, 'count_total': count_total, 'per_page': self._perPage,
                       'current_page': int(page)})

    def search(self, id):
        es = Elasticsearch()
        search_result = es.search(index="xdcr", body={"query": {"match": {"title": id}}})
        if len(search_result['hits']):
            ids = []
            for row in search_result['hits']['hits']:
                ids = ids + [row[u'_id']]

        c = self._getConnect();
        if len(ids):
            result = c.get_multi(ids)
        else:
            result = {}
        return render('search.html',
                      {'result': result, 'this': self, 'searchword': id})

    def edit(self, id):
        c = self._getConnect()
        id = id if self._isInt(id) and int(id) > 0 else 0
        if id == 0:
            object = {
                u'value': {
                    u'studios': [],
                    u'countries': [],
                    u'poster': '',
                    u'title': '',
                    u'years': [],
                    u'cast': {

                    }
                }
            }

            for ctype in self.getCastTypes():
                for name in ctype.value:
                    object[u'value'][u'cast'][name] = []
        else:
            object = c.get(id, quiet=True)

        return render('edit.html', {'id': id, 'object': object, 'connect': c})

    def add(self):
        return self.edit(0)

    def delete(self, id):
        id = id if self._isInt(id) and int(id) > 0 else 0
        if id:
            c = self._getConnect()
            c.delete(id)

        self._updateViews()

        return redirect(url(controller='admin', action='list', update=1))

    def save(self):
        c = self._getConnect()
        request = self._py_object.request
        post = request.POST
        poster = post.get('poster')
        old_obj = 0
        if int(post.get('id')):
            old_obj = c.get(post.get('id'))

        id = int(post.get('id')) or self._getNewId()

        countries = post.getall('countries[]') or 0
        studios = post.getall('studios[]') or 0
        years = post.getall('years[]') or 0
        casts = self._getCasts(request.POST)

        object = {
            u'title': post.get('title') or '',
        }

        if isinstance(poster, FieldStorage):
            poster_data = poster.file.read()
            static = httplib.HTTPConnection('static.local')
            filename = hashlib.md5(poster_data).hexdigest()
            extension = os.path.splitext(poster.filename)[1]
            object[u'poster'] = filename + extension
            static.request('PUT',
                           '/element/{0:s}/{1:s}/{2:s}{3:s}'.format(filename[0:2], filename[2:4], filename, extension),
                           poster_data)
        else:
            if old_obj and old_obj.value.get(u'poster'):
                object[u'poster'] = old_obj.value[u'poster']

        if casts:
            object[u'cast'] = {}

        if len(years):
            object[u'years'] = years

        if len(studios):
            object[u'studios'] = studios

        if len(countries):
            object[u'countries'] = countries
        for type in casts:
            # if not object[u'cast'].get(type):
            #     object[u'cast'][type] = []
            object[u'cast'][type] = casts[type]
        result_set = c.set('{0:d}'.format(id), object)

        self._updateViews()

        return redirect(url(controller='admin', action='list', update=1))

    def nid(self):
        exit(self._getNewId())

    def _getNewId(self):
        c = self._getIdsConnect()
        result = c.incr(g.database.get('ids_document'))
        return result.value

    def _updateViews(self):
        cbase = httplib.HTTPConnection('dbcouch.dev', 8092)
        cbase.request('GET', '/kryush/_design/dev_catalog/_view/list?stale=false')

        i = 0
        c = self._getConnect()
        for row in View(c, 'dev_catalog', 'list', stale=False):
            i = i + 1

    def _getCasts(self, data):
        output = {}
        for key in data:
            if re.match(u'^casts', key):
                f = re.search(u'casts\[(\w+)\]', key)
                if f:
                    if not output.get(f.group(1)):
                            output[f.group(1)] = []
                    for i in data.getall(key):
                        if i not in output[f.group(1)]:
                            output[f.group(1)].append(i)
        return output

    def _convertToDict(self, data):
        i = iter(data)
        result = {}
        for idx in i:
            result[idx] = data[idx]
        return result

    def _isInt(self, str):
        try:
            int(str)
        except ValueError:
            return False
        else:
            return True

    def _getCountTotal(self):
        c = self._getConnect()
        result = View(c, 'dev_catalog', 'list_cnt')
        total = 0
        for i in result:
            total = i.value
            break
        return total

    def getCastContent(self):
        request = self._py_object.request
        get = request.GET
        cast = get.get('cast') or ''
        c = self._getConnect()
        casts = View(c, 'dev_catalog', 'casts')
        for row in casts:
            for name in row.value:
                if name == cast:
                    return json.dumps(row.value[name])

        return json.dumps({})

    def getCastTypes(self):
        c = self._getConnect()
        casts = View(c, 'dev_catalog', 'casts')

        for row in casts:
            yield row

