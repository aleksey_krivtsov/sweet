"""Helper functions

Consists of functions to typically be used within templates, but also
available to Controllers. This module is available to templates as 'h'.
"""
# Import helpers as desired, or define your own, ie:
#from webhelpers.html.tags import checkbox, password


def _getCountry(c):
    from couchbase.views.iterator import View

    countries = View(c, 'dev_catalog', 'countries')
    for row in countries:
        for country in row.value:
            yield country

    return


def _getStudios(c):
    from couchbase.views.iterator import View

    studios = View(c, 'dev_catalog', 'studios')
    for row in studios:
        for studio in row.value:
            yield studio

    return


def _getCasts(c, type):
    _getCasts.needCache = False
    _getCasts._castsCache = 0
    from couchbase.views.iterator import View

    if _getCasts._castsCache == 0:
        needCache = True
        _getCasts._castsCache = []
        casts = View(c, 'dev_catalog', 'casts')
    else:
        casts = _getCasts._castsCache

    for row in casts:
        if needCache:
            _getCasts._castsCache = _getCasts._castsCache + [row]
        for cast in row.value[type]:
            yield cast
    return


def _getPicUrl(name, size=''):
    if not size:
        size = 'original/'

    size = '' #just for dev server TODO: remove
    return u'http://static.local/element/{0:s}{1:s}/{2:s}/{3:s}'.format(size, name[0:2], name[2:4],
                                                                         name) if name else ''


def getPager(per_page, pages_total):
    if per_page > pages_total:
        yield 1
    else:
        for i in range(1, int(round(pages_total / per_page) + (1 if pages_total % per_page > 0 else 0) + 1)):
            yield i

    return